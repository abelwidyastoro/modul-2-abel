# Marvel Travel

## Getting Started

### Prerequisites

1. Android Studio
2. Android Emulator
## Code

### HalamanUtama.java
```
  package com.example.marveltravel;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.marveltravel.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class HalamanUtama extends AppCompatActivity {
Dialog dialog;
Context context = this;
EditText isiSaldo, jumlahTiket;
TextView saldoku,tanggalMngkat,tanggalBalik,waktuMngkat,waktuBalik,plngpergi;
Spinner dataTujuan;
String[] daftarTujuan ={"Jakarta", "Cirebon","Bekasi"};
int [] hargaTiket = {85000,150000,70000};
String []list ;
SimpleDateFormat dateFormatter;
DatePickerDialog datePickerDialog;
TimePickerDialog timePickerDialog;
int tarifPulangPergi = 1;
public static int saldoSekarang = 0;
Switch mangkatMulih;
int BayarTiket;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_halaman_utama);
        saldoku = findViewById(R.id.inputSaldo);
        dataTujuan = findViewById(R.id.listTujuan);

        list = new String[daftarTujuan.length];
        for (int i = 0; i<daftarTujuan.length;i++){
            list[i]=daftarTujuan[i]+" (Rp. "+Integer.toString(hargaTiket[i])+"/orang)";
        }

        final ArrayAdapter<String> adapter=new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,list);
        dataTujuan.setAdapter(adapter);
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        tanggalMngkat = findViewById(R.id.tanggalID);
        waktuMngkat = findViewById(R.id.waktuIDberangkat);
        mangkatMulih = findViewById(R.id.bolakBalik);
        tanggalBalik = findViewById(R.id.tanggalBalikID);
        waktuBalik = findViewById(R.id.waktuBalikID);
        plngpergi = findViewById(R.id.plngpergi);
        PulangPergi(false);

        jumlahTiket = findViewById(R.id.banyakTiket);

        mangkatMulih.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                PulangPergi(isChecked);
            }
        });
    }
    public void setTopUp(View view){
    dialog = new Dialog(context);
    dialog.setContentView(R.layout.activity_top_up);
    isiSaldo = dialog.findViewById(R.id.inputSaldo);
    TextView tmbahSaldo, cancelSaldo;
    tmbahSaldo = dialog.findViewById(R.id.tmbahTopup);
    cancelSaldo = dialog.findViewById(R.id.cancelTopup);
    tmbahSaldo.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            saldoSekarang +=Integer.parseInt(isiSaldo.getText().toString());
            saldoku.setText(Integer.toString(saldoSekarang));
            dialog.dismiss();
        }
    });
    cancelSaldo.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            dialog.dismiss();
        }
    });
    dialog.show();
    }
    public void PilihTanggalBerangkat(View view){
        Calendar tanggalan = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year,month,dayOfMonth);
                tanggalMngkat.setText(dateFormatter.format(newDate.getTime()));
            }
        },tanggalan.get(Calendar.YEAR),tanggalan.get(Calendar.MONTH),tanggalan.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }
    public void PilihWaktuBerangkat(View view){
        Calendar tanggalan = Calendar.getInstance();
        timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                waktuMngkat.setText(hourOfDay+":"+minute);
            }
        },tanggalan.get(Calendar.HOUR_OF_DAY),tanggalan.get(Calendar.MINUTE), DateFormat.is24HourFormat(this));
        timePickerDialog.show();
    }
    public void PulangPergi(boolean isChecked){
        if (isChecked){
            plngpergi.setVisibility(View.VISIBLE);
            tanggalBalik.setVisibility(View.VISIBLE);
            waktuBalik.setVisibility(View.VISIBLE);
            tarifPulangPergi=2;
        }else{
            tarifPulangPergi=1;
            plngpergi.setVisibility(View.GONE);
            tanggalBalik.setVisibility(View.GONE);
            waktuBalik.setVisibility(View.GONE);
        }
        tanggalBalik.setText("Pilih Tanggal");
        waktuBalik.setText("Pilih Waktu");
        for (int i = 0;i<daftarTujuan.length;i++){
            list[i]=daftarTujuan[i]+" (Rp. "+Integer.toString(hargaTiket[i]*tarifPulangPergi)+"/orang)";
        }
        final ArrayAdapter<String>adapter=new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item,list);
        int sel = dataTujuan.getSelectedItemPosition();
        dataTujuan.setAdapter(adapter);
        dataTujuan.setSelection(sel);
    }
    public void PilihTanggalPulang(View view){
        Calendar tanggalan = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year,month,dayOfMonth);
                tanggalBalik.setText(dateFormatter.format(newDate.getTime()));
            }
        },tanggalan.get(Calendar.YEAR),tanggalan.get(Calendar.MONTH),tanggalan.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }
    public void PilihWaktuBalik(View view){
        Calendar tanggalan = Calendar.getInstance();
        timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                waktuBalik.setText(hourOfDay+":"+minute);
            }
        },tanggalan.get(Calendar.HOUR_OF_DAY),tanggalan.get(Calendar.MINUTE), DateFormat.is24HourFormat(this));
        timePickerDialog.show();
    }
    public void TukuTiket(View view){
        if (jumlahTiket.getText().toString().equalsIgnoreCase("")||Integer.parseInt(jumlahTiket.getText().toString())<=0){
            Toast.makeText(this, "Masukkan Banyak Tiket !", Toast.LENGTH_SHORT).show();
            jumlahTiket.requestFocus();
        }else{
            BayarTiket = hargaTiket[dataTujuan.getSelectedItemPosition()]*tarifPulangPergi*Integer.parseInt(jumlahTiket.getText().toString());
            if (saldoSekarang<BayarTiket) {
                Toast.makeText(this, "Saldo Anda tidak mencukupi, Silahkan Top up", Toast.LENGTH_SHORT).show();
            }else if(tanggalMngkat.getText().toString().equalsIgnoreCase("Pilih Tanggal")){
                Toast.makeText(context, "Pilih Tanggal keberangkatan", Toast.LENGTH_SHORT).show();
            }else if(tanggalBalik.getText().toString().equalsIgnoreCase("Pilih Tanggal")&&mangkatMulih.isChecked()){
                Toast.makeText(context, "Pilih Tanggal Kepulangan", Toast.LENGTH_SHORT).show();
            }else if(waktuMngkat.getText().toString().equalsIgnoreCase("Pilih Waktu")){
                Toast.makeText(context, "Pilih Waktu Keberangkatan", Toast.LENGTH_SHORT).show();
            }else if(waktuBalik.getText().toString().equalsIgnoreCase("Pilih Waktu")&&mangkatMulih.isChecked()){
                Toast.makeText(context, "Pilih Waktu Kepulangan", Toast.LENGTH_SHORT).show();
            }else {
                Intent kesimpulan = new Intent(this, Kesimpulan.class);
                kesimpulan.putExtra("tujuan", daftarTujuan[dataTujuan.getSelectedItemPosition()]);
                kesimpulan.putExtra("tanggalBerangkat", tanggalMngkat.getText().toString());
                kesimpulan.putExtra("waktuBerangkat", waktuMngkat.getText().toString());
                kesimpulan.putExtra("pulangPergi", mangkatMulih.isChecked());
                if (mangkatMulih.isChecked()) {
                    kesimpulan.putExtra("tanggalPulang", tanggalBalik.getText().toString());
                    kesimpulan.putExtra("waktuPulang", waktuBalik.getText().toString());
                }
                kesimpulan.putExtra("total",BayarTiket);
                kesimpulan.putExtra("jumlahTiket", jumlahTiket.getText().toString());
                startActivity(kesimpulan);
                onPause();
                Toast.makeText(this, "" + Integer.toString(BayarTiket), Toast.LENGTH_SHORT).show();
            }
        }
    }
       @Override
    protected void onResume() {
        super.onResume();
        if (!saldoku.getText().toString().equalsIgnoreCase("Rp " + Integer.toString(saldoSekarang))){
            tanggalMngkat.setText("Pilih Tanggal");
            tanggalBalik.setText("Pilih Tanggal");
            waktuMngkat.setText("Pilih Waktu");
            waktuBalik.setText("Pilih Waktu");
            jumlahTiket.setText("");
            PulangPergi(false);
        }
        refreshSaldo();
    }

    protected void refreshSaldo(){
        saldoku.setText(Integer.toString(saldoSekarang));
    }
}

```
### Kesimpulan.java
```
package com.example.marveltravel;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.marveltravel.R;

public class Kesimpulan extends AppCompatActivity {
    TextView tujuan,tanggalMangkat,tanggalPulang,totalHarga,jumlahTiket,pulangpergi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kesimpulan);
        tujuan = findViewById(R.id.tujuan);
        tanggalMangkat = findViewById(R.id.hasilTanggalMangkat);
        tanggalPulang = findViewById(R.id.hasilTanggalPulang);
        totalHarga = findViewById(R.id.hasilTotalHarga);
        jumlahTiket = findViewById(R.id.hasilJumlahTiket);
        if (!getIntent().getBooleanExtra("pulangPergi",false)){
            tanggalPulang.setVisibility(View.GONE);
        }else{
            tanggalPulang.setText(getIntent().getStringExtra("tanggalPulang")+" " + getIntent().getStringExtra("waktuPulang")+" WIB");
        }
        tujuan.setText(getIntent().getStringExtra("tujuan"));
        tanggalMangkat.setText(getIntent().getStringExtra("tanggalBerangkat")+" " + getIntent().getStringExtra("waktuBerangkat")+" WIB");
        jumlahTiket.setText(getIntent().getStringExtra("jumlahTiket"));

        totalHarga.setText(Integer.toString(getIntent().getIntExtra("total",0)));
    }
    public void confirm(View view){
        HalamanUtama.saldoSekarang-= getIntent().getIntExtra("total",0);
        finish();

    }
}

```
### MainActivity.java
```
package com.example.marveltravel;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;

import com.example.marveltravel.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        Thread x = new Thread(){
            public void run(){
                try {
                    sleep(3000);
                }catch (Exception e){
                    e.printStackTrace();
                }
                finally {
                    Intent pindahHome = new Intent(MainActivity.this, HalamanUtama.class);
                    startActivity(pindahHome);
                }
            }
        };
        x.start();
    }
    @Override
    protected void onPause(){
        super.onPause();
        finish();
    }
}

```
### activity_halaman_utama.xml
```
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context="com.example.marveltravel.HalamanUtama">

    <TextView
        android:id="@+id/judulSaldo"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginStart="16dp"
        android:layout_marginLeft="16dp"
        android:layout_marginTop="60dp"
        android:text="Jumlah  Saldo"
        android:textSize="24sp"
        android:textStyle="bold"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent" />

    <TextView
        android:id="@+id/textView3"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginStart="16dp"
        android:layout_marginLeft="16dp"
        android:text="Rp"
        android:textColor="@color/colorAccent"
        android:textSize="24sp"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/judulSaldo" />

    <TextView
        android:id="@+id/topUpId"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginStart="8dp"
        android:layout_marginLeft="8dp"
        android:layout_marginEnd="16dp"
        android:layout_marginRight="16dp"
        android:onClick="setTopUp"
        android:text="Top Up"
        android:textColor="@color/colorAccent"
        android:textSize="24sp"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="1.0"
        app:layout_constraintStart_toEndOf="@+id/inputSaldo"
        app:layout_constraintTop_toBottomOf="@+id/judulSaldo" />

    <TextView
        android:id="@+id/inputSaldo"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginStart="8dp"
        android:layout_marginLeft="8dp"
        android:text="0"
        android:textColor="@color/colorAccent"
        android:textSize="24sp"
        app:layout_constraintStart_toEndOf="@+id/textView3"
        app:layout_constraintTop_toBottomOf="@+id/judulSaldo" />

    <android.support.v7.widget.CardView
        android:id="@+id/cardView"
        android:layout_width="420dp"
        android:layout_height="wrap_content"
        android:layout_marginStart="8dp"
        android:layout_marginLeft="8dp"
        android:layout_marginTop="12dp"
        android:layout_marginEnd="8dp"
        android:layout_marginRight="8dp"
        app:cardBackgroundColor="@color/cardview_shadow_start_color"
        app:contentPadding="@dimen/cardview_compat_inset_shadow"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/inputSaldo">

        <android.support.constraint.ConstraintLayout
            android:layout_width="match_parent"
            android:layout_height="match_parent">

            <TextView
                android:id="@+id/textView7"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginStart="16dp"
                android:layout_marginLeft="16dp"
                android:layout_marginTop="16dp"
                android:text="Tujuan"
                android:textSize="24sp"
                android:textStyle="bold"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toTopOf="parent" />

            <Spinner
                android:id="@+id/listTujuan"
                android:layout_width="357dp"
                android:layout_height="24dp"
                android:layout_marginStart="32dp"
                android:layout_marginLeft="32dp"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/textView7" />

            <TextView
                android:id="@+id/textView8"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginStart="16dp"
                android:layout_marginLeft="16dp"
                android:text="Keberangkatan"
                android:textSize="24sp"
                android:textStyle="bold"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/listTujuan" />

            <TextView
                android:id="@+id/plngpergi"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginStart="16dp"
                android:layout_marginLeft="16dp"
                android:text="Kepulangan"
                android:textSize="24sp"
                android:textStyle="bold"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/bolakBalik" />

            <TextView
                android:id="@+id/tanggalID"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginStart="16dp"
                android:layout_marginLeft="16dp"
                android:onClick="PilihTanggalBerangkat"
                android:text="Pilih Tanggal"
                android:textColor="@color/colorAccent"
                android:textSize="18sp"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/textView8" />

            <TextView
                android:id="@+id/tanggalBalikID"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginStart="16dp"
                android:layout_marginLeft="16dp"
                android:onClick="PilihTanggalPulang"
                android:text="Pilih Tanggal"
                android:textColor="@color/colorAccent"
                android:textSize="18sp"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/plngpergi" />

            <TextView
                android:id="@+id/waktuIDberangkat"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginStart="16dp"
                android:layout_marginLeft="16dp"
                android:onClick="PilihWaktuBerangkat"
                android:text="Pilih Waktu"
                android:textColor="@color/colorAccent"
                android:textSize="18sp"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/tanggalID" />

            <TextView
                android:id="@+id/waktuBalikID"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginStart="16dp"
                android:layout_marginLeft="16dp"
                android:onClick="PilihWaktuBalik"
                android:text="Pilih Waktu"
                android:textColor="@color/colorAccent"
                android:textSize="18sp"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/tanggalBalikID" />

            <Switch
                android:id="@+id/bolakBalik"
                android:layout_width="351dp"
                android:layout_height="wrap_content"
                android:layout_marginStart="16dp"
                android:layout_marginLeft="16dp"
                android:text="Pulang Pergi"
                android:textSize="18sp"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/waktuIDberangkat" />

            <TextView
                android:id="@+id/textView13"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginStart="16dp"
                android:layout_marginLeft="16dp"
                android:layout_marginTop="40dp"
                android:text="Jumlah Tiket"
                android:textSize="24sp"
                android:textStyle="bold"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/waktuBalikID" />

            <EditText
                android:id="@+id/banyakTiket"
                android:layout_width="371dp"
                android:layout_height="wrap_content"
                android:layout_marginStart="8dp"
                android:layout_marginLeft="8dp"
                android:layout_marginTop="8dp"
                android:layout_marginEnd="8dp"
                android:layout_marginRight="8dp"
                android:ems="10"
                android:hint="Masukkan jumlah tiket"
                android:inputType="number"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintHorizontal_bias="0.516"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/textView13" />

        </android.support.constraint.ConstraintLayout>
    </android.support.v7.widget.CardView>

    <Button
        android:id="@+id/button"
        android:layout_width="400dp"
        android:layout_height="wrap_content"
        android:layout_marginStart="4dp"
        android:layout_marginLeft="4dp"
        android:layout_marginTop="8dp"
        android:layout_marginEnd="4dp"
        android:layout_marginRight="4dp"
        android:layout_marginBottom="4dp"
        android:onClick="TukuTiket"
        android:text="BELI TIKET"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/cardView"
        app:layout_constraintVertical_bias="1.0" />

</android.support.constraint.ConstraintLayout>
```
### activity_kesimpulan.xml
```
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context="com.example.marveltravel.Kesimpulan">

    <TextView
        android:id="@+id/textView2"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginStart="8dp"
        android:layout_marginLeft="8dp"
        android:layout_marginTop="8dp"
        android:layout_marginEnd="8dp"
        android:layout_marginRight="8dp"
        android:text="Summary"
        android:textSize="30sp"
        android:textStyle="bold"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent" />

    <android.support.v7.widget.CardView
        android:id="@+id/card2"
        android:layout_width="435dp"
        android:layout_height="wrap_content"
        android:layout_marginStart="8dp"
        android:layout_marginLeft="8dp"
        android:layout_marginTop="20dp"
        android:layout_marginEnd="8dp"
        android:layout_marginRight="8dp"
        app:cardBackgroundColor="@color/cardview_shadow_start_color"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.0"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/textView2">

        <android.support.constraint.ConstraintLayout
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:visibility="visible">

            <TextView
                android:id="@+id/textView6"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginStart="8dp"
                android:layout_marginLeft="8dp"
                android:layout_marginTop="8dp"
                android:text="Tujuan"
                android:textSize="24sp"
                android:textStyle="bold"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toTopOf="parent" />

            <TextView
                android:id="@+id/textView15"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginStart="8dp"
                android:layout_marginLeft="8dp"
                android:text="Tanggal Keberangkatan"
                android:textSize="24sp"
                android:textStyle="bold"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/tujuan" />

            <TextView
                android:id="@+id/textView17"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginStart="8dp"
                android:layout_marginLeft="8dp"
                android:text="Tanggal Pulang"
                android:textSize="24sp"
                android:textStyle="bold"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/hasilTanggalMangkat" />

            <TextView
                android:id="@+id/textView19"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginStart="8dp"
                android:layout_marginLeft="8dp"
                android:text="Jumlah Tiket"
                android:textSize="24sp"
                android:textStyle="bold"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/hasilTanggalPulang" />

            <TextView
                android:id="@+id/tujuan"
                android:layout_width="431dp"
                android:layout_height="32dp"
                android:layout_marginStart="8dp"
                android:layout_marginLeft="8dp"
                android:text="xx"
                android:textSize="24sp"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/textView6" />

            <TextView
                android:id="@+id/hasilTanggalMangkat"
                android:layout_width="401dp"
                android:layout_height="32dp"
                android:layout_marginStart="8dp"
                android:layout_marginLeft="8dp"
                android:text="xx"
                android:textSize="24sp"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/textView15" />

            <TextView
                android:id="@+id/hasilTanggalPulang"
                android:layout_width="398dp"
                android:layout_height="32dp"
                android:layout_marginStart="8dp"
                android:layout_marginLeft="8dp"
                android:text="xx"
                android:textSize="24sp"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/textView17" />

            <TextView
                android:id="@+id/hasilJumlahTiket"
                android:layout_width="404dp"
                android:layout_height="32dp"
                android:layout_marginStart="8dp"
                android:layout_marginLeft="8dp"
                android:text="xx"
                android:textSize="24sp"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/textView19" />
        </android.support.constraint.ConstraintLayout>
    </android.support.v7.widget.CardView>

    <TextView
        android:id="@+id/textView22"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginStart="8dp"
        android:layout_marginLeft="8dp"
        android:layout_marginTop="8dp"
        android:text="Total Harga"
        android:textSize="24sp"
        android:textStyle="bold"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/card2" />

    <TextView
        android:id="@+id/textView23"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginStart="208dp"
        android:layout_marginLeft="208dp"
        android:layout_marginTop="8dp"
        android:text="Rp"
        android:textColor="@color/colorAccent"
        android:textSize="24sp"
        android:textStyle="bold"
        app:layout_constraintStart_toEndOf="@+id/textView22"
        app:layout_constraintTop_toBottomOf="@+id/card2" />

    <TextView
        android:id="@+id/hasilTotalHarga"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginStart="4dp"
        android:layout_marginLeft="4dp"
        android:layout_marginTop="8dp"
        android:text="xx"
        android:textColor="@color/colorAccent"
        android:textSize="24sp"
        android:textStyle="bold"
        app:layout_constraintStart_toEndOf="@+id/textView23"
        app:layout_constraintTop_toBottomOf="@+id/card2" />

    <Button
        android:id="@+id/button2"
        android:layout_width="402dp"
        android:layout_height="wrap_content"
        android:layout_marginStart="8dp"
        android:layout_marginLeft="8dp"
        android:layout_marginEnd="8dp"
        android:layout_marginRight="8dp"
        android:layout_marginBottom="8dp"
        android:onClick="confirm"
        android:text="KONFIRMASI PEMBAYARAN"
        android:textStyle="bold"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent" />

</android.support.constraint.ConstraintLayout>
```
### activity_main.xml
```
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:background="#fff"
    tools:context="com.example.marveltravel.MainActivity">


    <TextView
        android:id="@+id/textView"
        android:layout_width="wrap_content"
        android:layout_height="51dp"
        android:layout_marginTop="8dp"
        android:layout_marginBottom="8dp"
        android:text="MARVEL TRAVEL"
        android:textColor="@color/colorAccent"
        android:textSize="30sp"
        android:textStyle="bold"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.5"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent" />

    <ImageView
        android:id="@+id/imageView"
        android:layout_width="236dp"
        android:layout_height="234dp"
        android:layout_marginStart="8dp"
        android:layout_marginLeft="8dp"
        android:layout_marginTop="8dp"
        android:layout_marginEnd="8dp"
        android:layout_marginRight="8dp"
        android:layout_marginBottom="8dp"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.503"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintVertical_bias="0.158"
        app:srcCompat="@drawable/mobil" />
</android.support.constraint.ConstraintLayout>
```
### activity_tp_up.xml
```
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="wrap_content">

    <TextView
        android:id="@+id/textView5"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginStart="8dp"
        android:layout_marginLeft="8dp"
        android:layout_marginTop="8dp"
        android:layout_marginEnd="8dp"
        android:layout_marginRight="8dp"
        android:text="Masukkan Jumlah Saldo"
        android:textColor="@android:color/background_dark"
        android:textSize="18sp"
        android:textStyle="bold"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent" />

    <TextView
        android:id="@+id/tmbahTopup"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginEnd="8dp"
        android:layout_marginRight="8dp"
        android:layout_marginBottom="16dp"
        android:text="TAMBAH SALDO"
        android:textColor="@color/colorPrimary"
        android:textStyle="bold"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent" />

    <TextView
        android:id="@+id/cancelTopup"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginTop="24dp"
        android:layout_marginEnd="16dp"
        android:layout_marginRight="16dp"
        android:layout_marginBottom="16dp"
        android:text="BATAL"
        android:textColor="@color/colorPrimary"
        android:textStyle="bold"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toStartOf="@+id/tmbahTopup"
        app:layout_constraintTop_toBottomOf="@+id/inputSaldo" />

    <EditText
        android:id="@+id/inputSaldo"
        android:layout_width="298dp"
        android:layout_height="wrap_content"
        android:layout_marginStart="8dp"
        android:layout_marginLeft="8dp"
        android:layout_marginTop="8dp"
        android:layout_marginEnd="8dp"
        android:layout_marginRight="8dp"
        android:ems="10"
        android:hint="Masukkan Saldo"
        android:inputType="number"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/textView5" />
</android.support.constraint.ConstraintLayout>
```

## Authors

  * Abel Nathalia Widyastoro