package com.example.marveltravel;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.marveltravel.R;

public class Kesimpulan extends AppCompatActivity {
    TextView tujuan,tanggalMangkat,tanggalPulang,totalHarga,jumlahTiket,pulangpergi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kesimpulan);
        tujuan = findViewById(R.id.tujuan);
        tanggalMangkat = findViewById(R.id.hasilTanggalMangkat);
        tanggalPulang = findViewById(R.id.hasilTanggalPulang);
        totalHarga = findViewById(R.id.hasilTotalHarga);
        jumlahTiket = findViewById(R.id.hasilJumlahTiket);
        if (!getIntent().getBooleanExtra("pulangPergi",false)){
            tanggalPulang.setVisibility(View.GONE);
        }else{
            tanggalPulang.setText(getIntent().getStringExtra("tanggalPulang")+" " + getIntent().getStringExtra("waktuPulang")+" WIB");
        }
        tujuan.setText(getIntent().getStringExtra("tujuan"));
        tanggalMangkat.setText(getIntent().getStringExtra("tanggalBerangkat")+" " + getIntent().getStringExtra("waktuBerangkat")+" WIB");
        jumlahTiket.setText(getIntent().getStringExtra("jumlahTiket"));

        totalHarga.setText(Integer.toString(getIntent().getIntExtra("total",0)));
    }
    public void confirm(View view){
        HalamanUtama.saldoSekarang-= getIntent().getIntExtra("total",0);
        finish();

    }
}
