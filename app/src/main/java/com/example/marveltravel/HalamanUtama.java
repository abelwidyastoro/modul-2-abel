package com.example.marveltravel;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.marveltravel.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class HalamanUtama extends AppCompatActivity {
Dialog dialog;
Context context = this;
EditText isiSaldo, jumlahTiket;
TextView saldoku,tanggalMngkat,tanggalBalik,waktuMngkat,waktuBalik,plngpergi;
Spinner dataTujuan;
String[] daftarTujuan ={"Jakarta", "Cirebon","Bekasi"};
int [] hargaTiket = {85000,150000,70000};
String []list ;
SimpleDateFormat dateFormatter;
DatePickerDialog datePickerDialog;
TimePickerDialog timePickerDialog;
int tarifPulangPergi = 1;
public static int saldoSekarang = 0;
Switch mangkatMulih;
int BayarTiket;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_halaman_utama);
        saldoku = findViewById(R.id.inputSaldo);
        dataTujuan = findViewById(R.id.listTujuan);

        list = new String[daftarTujuan.length];
        for (int i = 0; i<daftarTujuan.length;i++){
            list[i]=daftarTujuan[i]+" (Rp. "+Integer.toString(hargaTiket[i])+"/orang)";
        }

        final ArrayAdapter<String> adapter=new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,list);
        dataTujuan.setAdapter(adapter);
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        tanggalMngkat = findViewById(R.id.tanggalID);
        waktuMngkat = findViewById(R.id.waktuIDberangkat);
        mangkatMulih = findViewById(R.id.bolakBalik);
        tanggalBalik = findViewById(R.id.tanggalBalikID);
        waktuBalik = findViewById(R.id.waktuBalikID);
        plngpergi = findViewById(R.id.plngpergi);
        PulangPergi(false);

        jumlahTiket = findViewById(R.id.banyakTiket);

        mangkatMulih.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                PulangPergi(isChecked);
            }
        });
    }
    public void setTopUp(View view){
    dialog = new Dialog(context);
    dialog.setContentView(R.layout.activity_top_up);
    isiSaldo = dialog.findViewById(R.id.inputSaldo);
    TextView tmbahSaldo, cancelSaldo;
    tmbahSaldo = dialog.findViewById(R.id.tmbahTopup);
    cancelSaldo = dialog.findViewById(R.id.cancelTopup);
    tmbahSaldo.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            saldoSekarang +=Integer.parseInt(isiSaldo.getText().toString());
            saldoku.setText(Integer.toString(saldoSekarang));
            dialog.dismiss();
        }
    });
    cancelSaldo.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            dialog.dismiss();
        }
    });
    dialog.show();
    }
    public void PilihTanggalBerangkat(View view){
        Calendar tanggalan = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year,month,dayOfMonth);
                tanggalMngkat.setText(dateFormatter.format(newDate.getTime()));
            }
        },tanggalan.get(Calendar.YEAR),tanggalan.get(Calendar.MONTH),tanggalan.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }
    public void PilihWaktuBerangkat(View view){
        Calendar tanggalan = Calendar.getInstance();
        timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                waktuMngkat.setText(hourOfDay+":"+minute);
            }
        },tanggalan.get(Calendar.HOUR_OF_DAY),tanggalan.get(Calendar.MINUTE), DateFormat.is24HourFormat(this));
        timePickerDialog.show();
    }
    public void PulangPergi(boolean isChecked){
        if (isChecked){
            plngpergi.setVisibility(View.VISIBLE);
            tanggalBalik.setVisibility(View.VISIBLE);
            waktuBalik.setVisibility(View.VISIBLE);
            tarifPulangPergi=2;
        }else{
            tarifPulangPergi=1;
            plngpergi.setVisibility(View.GONE);
            tanggalBalik.setVisibility(View.GONE);
            waktuBalik.setVisibility(View.GONE);
        }
        tanggalBalik.setText("Pilih Tanggal");
        waktuBalik.setText("Pilih Waktu");
        for (int i = 0;i<daftarTujuan.length;i++){
            list[i]=daftarTujuan[i]+" (Rp. "+Integer.toString(hargaTiket[i]*tarifPulangPergi)+"/orang)";
        }
        final ArrayAdapter<String>adapter=new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item,list);
        int sel = dataTujuan.getSelectedItemPosition();
        dataTujuan.setAdapter(adapter);
        dataTujuan.setSelection(sel);
    }
    public void PilihTanggalPulang(View view){
        Calendar tanggalan = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year,month,dayOfMonth);
                tanggalBalik.setText(dateFormatter.format(newDate.getTime()));
            }
        },tanggalan.get(Calendar.YEAR),tanggalan.get(Calendar.MONTH),tanggalan.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }
    public void PilihWaktuBalik(View view){
        Calendar tanggalan = Calendar.getInstance();
        timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                waktuBalik.setText(hourOfDay+":"+minute);
            }
        },tanggalan.get(Calendar.HOUR_OF_DAY),tanggalan.get(Calendar.MINUTE), DateFormat.is24HourFormat(this));
        timePickerDialog.show();
    }
    public void TukuTiket(View view){
        if (jumlahTiket.getText().toString().equalsIgnoreCase("")||Integer.parseInt(jumlahTiket.getText().toString())<=0){
            Toast.makeText(this, "Masukkan Banyak Tiket !", Toast.LENGTH_SHORT).show();
            jumlahTiket.requestFocus();
        }else{
            BayarTiket = hargaTiket[dataTujuan.getSelectedItemPosition()]*tarifPulangPergi*Integer.parseInt(jumlahTiket.getText().toString());
            if (saldoSekarang<BayarTiket) {
                Toast.makeText(this, "Saldo Anda tidak mencukupi, Silahkan Top up", Toast.LENGTH_SHORT).show();
            }else if(tanggalMngkat.getText().toString().equalsIgnoreCase("Pilih Tanggal")){
                Toast.makeText(context, "Pilih Tanggal keberangkatan", Toast.LENGTH_SHORT).show();
            }else if(tanggalBalik.getText().toString().equalsIgnoreCase("Pilih Tanggal")&&mangkatMulih.isChecked()){
                Toast.makeText(context, "Pilih Tanggal Kepulangan", Toast.LENGTH_SHORT).show();
            }else if(waktuMngkat.getText().toString().equalsIgnoreCase("Pilih Waktu")){
                Toast.makeText(context, "Pilih Waktu Keberangkatan", Toast.LENGTH_SHORT).show();
            }else if(waktuBalik.getText().toString().equalsIgnoreCase("Pilih Waktu")&&mangkatMulih.isChecked()){
                Toast.makeText(context, "Pilih Waktu Kepulangan", Toast.LENGTH_SHORT).show();
            }else {
                Intent kesimpulan = new Intent(this, Kesimpulan.class);
                kesimpulan.putExtra("tujuan", daftarTujuan[dataTujuan.getSelectedItemPosition()]);
                kesimpulan.putExtra("tanggalBerangkat", tanggalMngkat.getText().toString());
                kesimpulan.putExtra("waktuBerangkat", waktuMngkat.getText().toString());
                kesimpulan.putExtra("pulangPergi", mangkatMulih.isChecked());
                if (mangkatMulih.isChecked()) {
                    kesimpulan.putExtra("tanggalPulang", tanggalBalik.getText().toString());
                    kesimpulan.putExtra("waktuPulang", waktuBalik.getText().toString());
                }
                kesimpulan.putExtra("total",BayarTiket);
                kesimpulan.putExtra("jumlahTiket", jumlahTiket.getText().toString());
                startActivity(kesimpulan);
                onPause();
                Toast.makeText(this, "" + Integer.toString(BayarTiket), Toast.LENGTH_SHORT).show();
            }
        }
    }
       @Override
    protected void onResume() {
        super.onResume();
        if (!saldoku.getText().toString().equalsIgnoreCase("Rp " + Integer.toString(saldoSekarang))){
            tanggalMngkat.setText("Pilih Tanggal");
            tanggalBalik.setText("Pilih Tanggal");
            waktuMngkat.setText("Pilih Waktu");
            waktuBalik.setText("Pilih Waktu");
            jumlahTiket.setText("");
            PulangPergi(false);
        }
        refreshSaldo();
    }

    protected void refreshSaldo(){
        saldoku.setText(Integer.toString(saldoSekarang));
    }
}
