package com.example.marveltravel;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;

import com.example.marveltravel.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        Thread x = new Thread(){
            public void run(){
                try {
                    sleep(3000);
                }catch (Exception e){
                    e.printStackTrace();
                }
                finally {
                    Intent pindahHome = new Intent(MainActivity.this, HalamanUtama.class);
                    startActivity(pindahHome);
                }
            }
        };
        x.start();
    }
    @Override
    protected void onPause(){
        super.onPause();
        finish();
    }
}
